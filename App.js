import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import OneSignal from 'react-native-onesignal';

class App extends Component {
  componentDidMount() {
    OneSignal.init('70775b65-d580-487f-bfc9-e4e5e94c8183');
    OneSignal.addEventListener('received', (data) => {
      console.log(data);
    });
    OneSignal.inFocusDisplaying(2);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received');
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>App</Text>
      </View>
    );
  }
}
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
